order grid 添加 Payment method
1. collection
2. column






TODO: 允许在后台填加来源：table source, 字段有 url的referer含的string source的label


First of all you can create extension which will save order_id and source_type in separate table. 
For example yourextension_order_source_type. 
After you will create observer to listen to sales_order_place_after event. 
Now every time when order is creating you can get somehow source type and save it with order_id.

To display it you can just create your own block and add it to order edit page.

create your own module for this task rather than editing the core files. 
Add this line to your _prepareCollection function in you grid.php file

$collection = Mage::getResourceModel('sales/order_collection')
        ->join(array('a' => 'sales/order_address'), 'main_table.entity_id = a.parent_id AND a.address_type != \'billing\'', array(
            'city'       => 'city',
            'country_id' => 'country_id'
        ))
Add this lines to function _prepareColumns in grid.php file

$this->addColumn('city', array(
        'header' => $helper->__('City'),
        'index'  => 'city'
    ));
hope it will work.


No need to rewrite, can all be done in an observer. In the following code, I used the event: adminhtml_block_html_before

The code below is to insert two new columns, called 'Referred By' and a lookup list one for 'State', into the Newsletter Grid.

The idea is the same, and you should be able to use it in your own grid customizations.

The code also includes a means to fix/enable filtering of the column


public function adminhtml_block_html_before(Varien_Event_Observer $observer)
    {

        try {
            $event = $observer->getEvent();
            if ($event->getBlock() instanceof
                Mage_Adminhtml_Block_Newsletter_Subscriber_Grid 
            ) {
                $this->_grid = $event->getBlock();
                $this->_collection = $this->_grid->getCollection();

                /** 
                    YOU COULD ADJUST COLLECTION HERE
                    ADD NEW FIELD FILTERS< COLUMNS ETC **/

                $columnData = array(
                    'header' => 'Referred By',
                    'index'  => 'referred_by',
                    'type'   => 'text',
                );

                $this->_grid->addColumn('referred_by', $columnData);
                $options = array();
                $regions = mage::helper('proxiblue_enhancedsubscription')->getRegionCollection();

                foreach ($regions as $row) {
                    $options[$row['code']] = $row['name'];
                }

                $columnData = array(
                    'header' => 'State',
                    'index'  => 'state',
                    'type' => 'options',
                    'options' => $options,
                    'width' => 400
                );

                $this->_grid->addColumnAfter('state', $columnData, 'lastname');
                $this->_grid->sortColumnsByOrder();
                // rebuild the filters
                $filter = $this->_grid->getParam($this->_grid->getVarNameFilter(), null);
                if (is_null($filter)) {
                    return $this;
                }
                $this->_collection->clear();
                if (is_string($filter)) {
                    $data = $this->_grid->helper('adminhtml')->prepareFilterString($filter);
                    $this->_setFilterValues($data);
                } else {
                    if ($filter && is_array($filter)) {
                        $this->_setFilterValues($filter);
                    }
                }
                // force a reload of the collection
                $this->_collection->load();
            }
        } catch (Exception $e) {
            mage::logException($e);
        }

        return $this;
    }

    protected function _setFilterValues($data)
    {
        foreach ($this->_grid->getColumns() as $columnId => $column) {
            if (isset($data[$columnId]) && (!empty($data[$columnId]) || strlen($data[$columnId]) > 0)
                && $column->getFilter()
            ) {
                $column->getFilter()->setValue($data[$columnId]);
                $this->_addColumnFilterToCollection($column);
            }
        }

        return $this;
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($this->_collection) {
            $field = ($column->getFilterIndex()) ? $column->getFilterIndex() : $column->getIndex();
            if ($column->getFilterConditionCallback()) {
                call_user_func($column->getFilterConditionCallback(), $this->getCollection(), $column);
            } else {
                $cond = $column->getFilter()->getCondition();
                if ($field && isset($cond)) {
                    $this->_collection->addFieldToFilter($field, $cond);
                }
            }
        }

        return $this;
    }
shareimprove this answer
answered Jun 26 '15 at 17:25

ProxiBlue
7,99422148
  	 	
Love this, thank you it is awesome. I've reworked this to my own needs to have a PHP trait with one public method (so far) resetWidgetGridFilters($grid) that takes care of all the reapplying of filters to the grid and collection object. Now in my observer I just add the column, resort the order, and pass the grid object to my PHP trait. One small bug though I think in your current logic is call_user_func($column->getFilterConditionCallback(), $this->getCollection(), $column);. I think $this->getCollection() is supposed to be $this->_collection. – Meogi Apr 5 '16 at 20:15
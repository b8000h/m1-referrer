<?php

class Nullor_Referrer_Model_Observer
{
    const XML_PATH_NULLOR_REFERRER_GENERAL_REFERRERS = 'nullor_referrer/general/referrers';

    public function addCookie($observer)
    {
        /* for debug
        Mage::getSingleton('core/cookie')->set( 'referrer', '1', 3600 * 24 * 30 , '/', null, null, true );
        setcookie('flavor','chocolate chip');
        echo Mage::getSingleton('core/cookie')->get('referrer');
        die();
        */
        
        $referrer = $observer->getEvent()->getData('front')->getRequest()->getParam('referrer');
        if ($referrer) {
            Mage::app()->getCookie()->set('referrer', $referrer);
        } else {
            $http_referer = $observer->getEvent()->getData('front')->getRequest()->getServer('HTTP_REFERER');
            $http_referer = parse_url($http_referer);
            $http_referer_host = $http_referer['host'];
            // 就算在自家站内点击超链接，HTTP_REFERER 亦会实时记录，覆盖
            // 所以写个检测 HTTP_REFERER 的函数是必要的

            /*
            //$referrers = array();
            $referrers_string = Mage::getStoreConfig(self::XML_PATH_NULLOR_REFERRER_GENERAL_REFERRERS);
            $referrers = explode(',', $referrers_string);

            if( is_array($referrers) && !empty($referrers) ){
                foreach( $referrers as $referrer ){
                    if($referrer){
                        if( strpos($http_referer, $referrer) ){
                            Mage::app()->getCookie()->set('referrer',$referrer);
                            break;
                        }
                    }
                }
            }
            */

            $base_url = parse_url(Mage::getBaseUrl());
            $base_url = $base_url['host'];

            if (strpos($http_referer_host, $base_url) === false) {
                Mage::app()->getCookie()->set('referrer', $http_referer_host);
            }
        }
        

        //$_cookie_tag = Mage::helper('nullor_referrer')->getCookieTag();
        $_cookie_lifetime = Mage::helper('nullor_referrer')->getCookieLifetime();
        //$_referrer_keyword = Mage::helper('nullor_referrer')->getReferrerKeyword();

        if (Mage::helper('nullor_referrer')->isEnabled()) {
            // 既然你的设计是下单即删 cookie，则其过期时间不再重要
            if ($_cookie_lifetime) {
                $cookie_lifetime = 3600 * 24 * $_cookie_lifetime;
            } else {
                $cookie_lifetime = 3600 * 24 * 30;
            }

            //if ( strpos($referrer, $_referrer_keyword) ) {
                //Mage::app()->getCookie()->set('referrer',$http_referer);
            //}
        }

        // 其实在 Chrome 开发者工具的 Application > Storage > Cookies 也能看
        //if (Mage::helper('nullor_referrer')->isDebug()){
            // 在页面顶部输出，但是会影响页面字体(变大)，原因不明
            //echo 'Cookie referrer = ' . Mage::getSingleton('core/cookie')->get('referrer');
        //}
    }

    public function addReferrer($observer)
    {
        //$_referrer_name = Mage::helper('nullor_referrer')->getReferrerName();
        //$_cookie_tag = Mage::helper('nullor_referrer')->getCookieTag();
        $_order_increment_id = $observer->getOrder()->getIncrementId();

        if (Mage::helper('nullor_referrer')->isDebug()) {
            Mage::app()->getCookie()->set('order_id', $_order_increment_id);
        }
        //Mage::app()->getCookie()->set('order_id',dump_var($observer->getOrder())); // 失败

        //$observer->getOrder()->setReferrer('yiguo')->save();

        //$pollModel = Mage::getModel('sales/order')->load(95);
        //$pollModel->setReferrer('yiguo')->save();
        

        $referrer = Mage::app()->getCookie()->get('referrer');
        if ($referrer) {
            //$order = Mage::getModel('sales/order')->loadByIncrementId($_order_increment_id);
            //$order->setReferrer($referrer)->save();
            $observer->getOrder()->setReferrer($referrer)->save();
            
            Mage::app()->getCookie()->set('referrer', '');
        }
    }


//--------------------------------------------------------------------------------------------------

    // 留待新 table 实现
    public function sourceRecord($observer)
    {
        /* 测试 event 监听的成功与否
        setcookie('test', 'test'); // 不知为何 setcookie 的写法不行
        Mage::getSingleton('core/cookie')->set( 'tt', 'tt', 3600 * 24 * 30 , '/', null, null, true );
        */
        
        $_order_id = $observer->getOrder()->getIncrementId(); // getId() 不行，会报错
        $_referrer_name = Mage::helper('nullor_referrer')->getReferrerName();
        $_cookie_tag = Mage::helper('nullor_referrer')->getCookieTag();

        // cookie 名不能带空格，如不能叫 'order id'
        /* 测试各变量的获取的成功与否
        Mage::getSingleton('core/cookie')->set( 'order_id', $_order_id, 3600*24*30 , '/', null, null, true );
        Mage::getSingleton('core/cookie')->set( 'referrer_name', $_referrer_name, 3600*24*30 , '/', null, null, true );
        Mage::getSingleton('core/cookie')->set( 'cookie_tag', $_cookie_tag, 3600*24*30 , '/', null, null, true );
        */

        $_referrer = Mage::getModel('nullor_referrer/referrer');

        // for debug
        $_referrer->setEntityId('1')->setOrderId($_order_id)->setReferrer($_referrer_name);
        //Mage::getSingleton('core/cookie')->set('referrer_id', $_referrer->getId(), 3600*24*30, '/', null, null, true);
        Mage::getSingleton('core/cookie')->set('order_id', $_referrer->getOrderId(), 3600*24*30, '/', null, null, true);
        Mage::getSingleton('core/cookie')->set('referrer', $_referrer->getReferrer(), 3600*24*30, '/', null, null, true);
        try {
            $_referrer->save();
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
        }
       

        /*
        if ( Mage::getSingleton('core/cookie')->get('referrer') ){
            $_referrer->setOrderId($_order_id)->setReferrer($_referrer_name);
            $_referrer->save();
        }
        */
    }

    // 留待新table实现
    // 这种写法并没有处理好新增列的 search 功能
    public function addGrid(Varien_Event_Observer $observer)
    {
        try {
            $event = $observer->getEvent();
            if ($event->getBlock() instanceof Mage_Adminhtml_Block_Sales_Order_Grid) {
                $this->_grid = $event->getBlock();

                //$this->_collection = $this->_grid->getCollection();
                // 此类写法尽在各 Resource/
                /*
                $this->_collection = Mage::getResourceModel('sales/order_collection')
                    ->join(array('a' => 'nullor_referrer/nullor_referrer'), 'main_table.entity_id = a.order_id', array(
                        'referrer'       => 'referrer'
                    ));
                */

                /* 会排在 Action 的后面
                $this->_grid->addColumn('referrer', array(
                    'header' => Mage::helper('sales')->__('Referrer'),
                    'index'  => 'referrer',
                ));
                */

                $this->_grid->addColumnAfter('referrer', array(
                    'header' => Mage::helper('sales')->__('Referrer'),
                    'index'  => 'referrer',
                    'sortable' => true,
                    ), 'status');

                /**
                    YOU COULD ADJUST COLLECTION HERE
                    ADD NEW FIELD FILTERS< COLUMNS ETC **/


                /*
                $options = array();
                $regions = mage::helper('proxiblue_enhancedsubscription')->getRegionCollection();

                foreach ($regions as $row) {
                    $options[$row['code']] = $row['name'];
                }

                $columnData = array(
                    'header' => 'State',
                    'index'  => 'state',
                    'type' => 'options',
                    'options' => $options,
                    'width' => 400
                );

                $this->_grid->addColumnAfter('state', $columnData, 'lastname');
                 */



                $this->_grid->sortColumnsByOrder();

                // rebuild the filters
                $filter = $this->_grid->getParam($this->_grid->getVarNameFilter(), null);
                if (is_null($filter)) {
                    return $this;
                }
                $this->_collection->clear();
                if (is_string($filter)) {
                    $data = $this->_grid->helper('adminhtml')->prepareFilterString($filter);
                    $this->_setFilterValues($data);
                } else {
                    if ($filter && is_array($filter)) {
                        $this->_setFilterValues($filter);
                    }
                }
                // force a reload of the collection
                $this->_collection->load();
            }
        } catch (Exception $e) {
            mage::logException($e);
        }

        return $this;
    }

    protected function _setFilterValues($data)
    {
        foreach ($this->_grid->getColumns() as $columnId => $column) {
            if (isset($data[$columnId]) && (!empty($data[$columnId]) || strlen($data[$columnId]) > 0)
                && $column->getFilter()
            ) {
                $column->getFilter()->setValue($data[$columnId]);
                $this->_addColumnFilterToCollection($column);
            }
        }

        return $this;
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($this->_collection) {
            $field = ($column->getFilterIndex()) ? $column->getFilterIndex() : $column->getIndex();
            if ($column->getFilterConditionCallback()) {
                call_user_func($column->getFilterConditionCallback(), $this->_collection, $column);
            } else {
                $cond = $column->getFilter()->getCondition();
                if ($field && isset($cond)) {
                    $this->_collection->addFieldToFilter($field, $cond);
                }
            }
        }

        return $this;
    }
}

<?php

class Nullor_Referrer_Model_Resource_Referrer extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('nullor_referrer/referrer', 'entity_id');
    }
}
<?php 
/**
 * 用于后台设置的开关，职责单一明确
 * 放方便获取设置值的 methods
 * 
 */
class Nullor_Referrer_Helper_Data extends Mage_Core_Helper_Abstract
{

    //Config paths
    const MODULE_ENABLED            = 'nullor_referrer/general/enabled';
    const DEBUG                     = 'nullor_referrer/general/debug';
    const COOKIE_LIFETIME           = 'nullor_referrer/general/cookie_lifetime';

    
	public function isEnabled($store = null)
    {
        // TODO: 必须的设置项不为空才能激活使用
        return Mage::getStoreConfig(self::MODULE_ENABLED, $store);
    }

    public function isDebug($store = null)
    {
        return $this->isEnabled($store) && Mage::getStoreConfig(self::DEBUG, $store);
    }


    public function getCookieLifetime($store = null)
    {
        return Mage::getStoreConfig(self::COOKIE_LIFETIME, $store);
    }
}
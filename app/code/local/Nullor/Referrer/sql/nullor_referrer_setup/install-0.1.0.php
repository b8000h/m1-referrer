<?php

//--------------------------------------------------------------------------------------------------
// 第一版，直接在 table sales_flat_order 加一列，添加成功
// 须同时在 sales_flat_order_grid 加一列，似乎这个 table 是 ce 1.9 只有才有的，旧的教程(至少2012年的)不会提到这个 
// 他俩之间的数据会自行同步
    // 想要在 Grid 能搜索就必须有 Index

$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'referrer', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'    => 255,
        'nullable'  => true,
        'comment'   => 'Referrer'
    ));
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_grid'), 'referrer', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'    => 255,
        'nullable'  => true,
        'comment'   => 'Referrer'
    ));

// 不是唯一值，所以无需设为 index
$installer->getConnection()
    ->addIndex(
        $installer->getTable('sales/order_grid'),
        $installer->getIdxName('sales/order_grid', array('referrer')),
        array('referrer')
    );

$installer->endSetup();


//--------------------------------------------------------------------------------------------------
// 第二版，另起一 table

/*

$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('nullor_referrer/nullor_referrer'))
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Entity Id')
    ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        ), 'Order Id')
    ->addColumn('referrer', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        ), 'Referrer')
    ->addIndex($installer->getIdxName('nullor_referrer/nullor_referrer', array('order_id')),
        array('order_id'))
    // 这段代码导致了下面的报错，待查明
    // 仿 customer_entity 和 customer_address_entity
    //->addForeignKey($installer->getFkName('nullor_referrer/nullor_referrer', 'order_id', 'sales/order', 'entity_id'),
        'order_id', $installer->getTable('sales/order'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Referrer');
$installer->getConnection()->createTable($table);

$installer->endSetup();
*/


/**
https://stackoverflow.com/questions/14481684/1005-cant-create-table-errno-150-magento

Error in file: "/vagrant/httpdocs/m1/m1932/app/code/local/Nullor/Ordersource/sql/ordersource_setup/install-0.1.0.php" - SQLSTATE[HY000]: General error: 1005 Can't create table 'm1932.order_source' (errno: 150), query was: CREATE TABLE `order_source` (
  `entity_id` int UNSIGNED NOT NULL auto_increment COMMENT 'Entity Id' ,
  `order_id` varchar(255) NULL COMMENT 'Order id' ,
  `order_source` varchar(255) NULL COMMENT 'Order Source' ,
  PRIMARY KEY (`entity_id`),
  INDEX `IDX_ORDER_SOURCE_ORDER_ID` (`order_id`),
  CONSTRAINT `FK_ORDER_SOURCE_ORDER_ID_SALES_FLAT_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE SET NULL ON UPDATE CASCADE
) COMMENT='Order Source' ENGINE=INNODB charset=utf8 COLLATE=utf8_general_ci

#0 /vagrant/httpdocs/m1/m1932/app/code/core/Mage/Core/Model/Resource/Setup.php(644): Mage::exception('Mage_Core', 'Error in file: ...')
#1 /vagrant/httpdocs/m1/m1932/app/code/core/Mage/Core/Model/Resource/Setup.php(421): Mage_Core_Model_Resource_Setup->_modifyResourceDb('install', '', '0.1.0')
#2 /vagrant/httpdocs/m1/m1932/app/code/core/Mage/Core/Model/Resource/Setup.php(327): Mage_Core_Model_Resource_Setup->_installResourceDb('0.1.0')
#3 /vagrant/httpdocs/m1/m1932/app/code/core/Mage/Core/Model/Resource/Setup.php(235): Mage_Core_Model_Resource_Setup->applyUpdates()
#4 /vagrant/httpdocs/m1/m1932/app/code/core/Mage/Core/Model/App.php(428): Mage_Core_Model_Resource_Setup::applyAllUpdates()
#5 /vagrant/httpdocs/m1/m1932/app/code/core/Mage/Core/Model/App.php(354): Mage_Core_Model_App->_initModules()
#6 /vagrant/httpdocs/m1/m1932/app/Mage.php(684): Mage_Core_Model_App->run(Array)
#7 /vagrant/httpdocs/m1/m1932/index.php(83): Mage::run('', 'store')
#8 {main}
*/



/*
应为 ordersource/order_source
*/

/**
Error in file: "/vagrant/httpdocs/m1/m1932/app/code/local/Nullor/Ordersource/sql/ordersource_setup/install-0.1.0.php" - Can't retrieve entity config: nullor/order_source

#0 /vagrant/httpdocs/m1/m1932/app/code/core/Mage/Core/Model/Resource/Setup.php(644): Mage::exception('Mage_Core', 'Error in file: ...')
#1 /vagrant/httpdocs/m1/m1932/app/code/core/Mage/Core/Model/Resource/Setup.php(421): Mage_Core_Model_Resource_Setup->_modifyResourceDb('install', '', '0.1.0')
#2 /vagrant/httpdocs/m1/m1932/app/code/core/Mage/Core/Model/Resource/Setup.php(327): Mage_Core_Model_Resource_Setup->_installResourceDb('0.1.0')
#3 /vagrant/httpdocs/m1/m1932/app/code/core/Mage/Core/Model/Resource/Setup.php(235): Mage_Core_Model_Resource_Setup->applyUpdates()
#4 /vagrant/httpdocs/m1/m1932/app/code/core/Mage/Core/Model/App.php(428): Mage_Core_Model_Resource_Setup::applyAllUpdates()
#5 /vagrant/httpdocs/m1/m1932/app/code/core/Mage/Core/Model/App.php(354): Mage_Core_Model_App->_initModules()
#6 /vagrant/httpdocs/m1/m1932/app/Mage.php(684): Mage_Core_Model_App->run(Array)
#7 /vagrant/httpdocs/m1/m1932/index.php(83): Mage::run('', 'store')
#8 {main}
*/

/**
Error in file: "/vagrant/httpdocs/m1/m1932/app/code/local/Nullor/Ordersource/sql/ordersource_setup/install-0.1.0.php" - Can't retrieve entity config: order_source/order_source
*/